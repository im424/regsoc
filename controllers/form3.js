var mysql = require('../db');
var User = require('../models/user');
var Upload_File  = require('../models/uploadFile');
var Form3  = require('../models/form3');
var whichForm = null;
var formNameList = [];
var whichForminFrom3 = null;
var reportType = null;

exports.adminform3 = function(req,res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(!isLogin){
			console.log('not login');
			return res.redirect('login');
		}
		switch(req.query.form3){
			case 'A':
				whichForm = "去屆工作報告";
				reportType = "report";
				type_ref_no = "report"+"_ref_no";
				whichForminFrom3 = '/form3?form3=A';
				break;
			case 'B':
				whichForm = "去屆財政報告";
				reportType = "financialReport";
				type_ref_no = "financial_report"+"_ref_no";
				whichForminFrom3 = '/form3?form3=B';
				break;
			case 'C':
				whichForm = "現屆工作計劃";
				reportType = "yearPlan";
				type_ref_no = "year_plan"+"_ref_no";
				whichForminFrom3 = '/form3?form3=C';
				break;
			case 'D':
				whichForm = "現屆財政預算";
				reportType = "budget";
				type_ref_no = "budget"+"_ref_no";
				whichForminFrom3 = '/form3?form3=D';
				break;
			default:
				res.write('Error - Form 3_ is missing');
				break;
		}

		Form3.AdminInit(req,res,type_ref_no,req.query.form3,function(rsd,motion,record){
			if(rsd != "noRecord"){
				res.render('admin/regsoc_form3',{
					IsAdmin : isAdmin,
					loginStatus : isLogin,
					whichForm : whichForm,
					reportType : reportType,
					rsd : rsd,	
					motion: motion ,
					record: record 
					});
			}else{
				res.send('No record');
			}
			});
		});
}

exports.Enterform3 = function(req, res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(!isLogin){
			console.log('not login');
			return res.redirect('login');
		}
		switch(req.query.form3){
			case 'A':
				whichForm = "去屆工作報告";
				reportType = "report";
				type_ref_no = "report"+"_ref_no";
				whichForminFrom3 = '/form3?form3=A';
				break;
			case 'B':
				whichForm = "去屆財政報告";
				reportType = "financialReport";
				type_ref_no = "financial_report"+"_ref_no";
				whichForminFrom3 = '/form3?form3=B';
				break;
			case 'C':
				whichForm = "現屆工作計劃";
				reportType = "yearPlan";
				type_ref_no = "year_plan"+"_ref_no";
				whichForminFrom3 = '/form3?form3=C';
				break;
			case 'D':
				whichForm = "現屆財政預算";
				reportType = "budget";
				type_ref_no = "budget"+"_ref_no";
				whichForminFrom3 = '/form3?form3=D';
				break;
			default:
				res.write('Error - Form 3_ is missing');
				break;
		}
		Form3.Form3Init(req,res,req.query.form3,function(rsd,motion,record){
			res.render('form3',{
				IsAdmin : isAdmin,
				loginStatus : isLogin,
				whichForm : whichForm,
				reportType : reportType,
				rsd : rsd,	
				motion: motion ,
				record: record 
			});
		});
	});
};


exports.submit_form3 =  function(req, res) {
	var refNo = null;
	switch(req.body['reportType']){
			case 'yearPlan':
				type_ref_no = "year_plan"+"_ref_no";
				whichForminFrom3 = '/form3?form3=C';
				break;
			case 'budget':
				type_ref_no = "budget"+"_ref_no";
				whichForminFrom3 = '/form3?form3=D';
				break;
			case 'report':
				type_ref_no = "report"+"_ref_no";
				whichForminFrom3 = '/form3?form3=A';
				break;
			case 'financialReport':
				type_ref_no = "financial_report"+"_ref_no";
				whichForminFrom3 = '/form3?form3=B';
				break;
			default:
				console.log("Form 3 report type error");
				return;
	}

	Form3.submitForm3(req,res,type_ref_no,whichForminFrom3,function(success){
		if(success){
			res.render( 'submit_form3',{
				upload_status : '成功',
				upload_message : '成功更新資料',
				return_path : 'form3',
				IsAdmin : isAdmin,
				loginStatus : isLogin
			});
		}else{
			return res.redirect( 'form3');
		}
	});
};