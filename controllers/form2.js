var mysql = require('../db');
var Form2 = require('../models/form2');
var User = require('../models/user');


exports.Enterform2 = function(req, res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(!isLogin){
			console.log('not login');
			return res.redirect('login');
		}
		Form2.Form2Init(req,res,function(exco,excoMembers){
			res.render('form2',{
				IsAdmin : isAdmin,
				loginStatus : isLogin,
				exco: exco,
				excoMembers: excoMembers
			});

		});
	});
};


exports.submit_form2 = function(req, res) {
	Form2.submitForm2(req,res,function(success,number){
		if(success){
			User.checkLoginStatus(req,res, function(isLogin,isAdmin){
				res.render( 'submit_form2',{
					submit_status : '成功',
					post_message : '成功新增 ' + number+' 位幹事會成員',
					return_path : 'form2',
					IsAdmin : isAdmin,
					loginStatus : isLogin
				});
			});
		}else{
			return res.redirect( 'form2');
		}
	});
};

exports.adminform2 =function(req, res){

	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
	if(!isLogin){
		console.log('not login');
		return res.redirect('login');
	}
	Form2.AdminInit(req,res,function(exco,excoMembers){
		if(exco=="NA"){
			res.send('no record');
			return;
		}
		res.render('admin/regsoc_form2',{
			IsAdmin : isAdmin,
			loginStatus : isLogin,
			exco: exco,
			excoMembers: excoMembers
		});


		});
});
	}