var isLogin = false;
var isAdmin = false;
var AdminList = [];
var loginError = 0; //0->success , 1->wrong password , 2->no this user, 3->recaptcha
var total_file = 0;

var mysql = require('../db');
var Upload_File  = require('../models/uploadFile');

var https = require('https');


mysql.connect(function(err){
	if(!err){
		console.log("Database is conneccted...");
	}else{
		console.log("Error connecting databases");
	}
});

function whoIsAdmin(){
	var admin = [];
	mysql.query('select email,soc_code from User',function(err,rows,fields){
		if(err){
			console.log('error');
		}else{
			for(var i in rows){
				if(rows[i].soc_code=='admin'){
					admin.push(rows[i].email);
				}
			}
		}
		AdminList = admin;
	});
}
whoIsAdmin(); //list all the admin and store to AdminList


var checkLoginStatus = function(req, res){
	isLogin = false;
	isAdmin = false;
	if(req.signedCookies.userid){
		isLogin = true;
	}
	for(var i in AdminList){
		if(req.signedCookies.userid==AdminList[i]){
			isAdmin = true;
			break;
		}
	}
	console.log('isAdmin: '+isAdmin);
	// if(req.signedCookies.userid && req.signedCookies.password){
	// 	isLogin = true;
	// }
};
// function uploadFile (req,res,upload_field,target_des,current_file,total_file,return_to,show_page){

// 	if(upload_field!=undefined){//upload_field==undefined when there is no more new field for file uploading in that form
// moved to ../models/uploadFile
// };




//首頁
exports.index = function(req, res){
	checkLoginStatus(req,res);
	if(!isLogin){
		res.render('login',{
			IsAdmin : isAdmin,
			loginStatus : isLogin,
			loginErrorCode : loginError
		});
	}else{
		res.render('index',{
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	}
	loginError = 0;
};

exports.login = function(req, res){
	checkLoginStatus(req,res);
	console.log('Login: username?:'+req.signedCookies.userid);
	res.render( 'login',{
		IsAdmin : isAdmin,
		loginStatus : isLogin,
		loginErrorCode : loginError
	} );	
};

var SECRET = "6LcCwQMTAAAAAIkqVufoj6ULNOuIR-s7afbbsJSC";

function verifyRecaptcha(key, callback) {
        https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET + "&response=" + key, function(res) {
                var data = "";
                res.on('data', function (chunk) {
                        data += chunk.toString();
                });
                res.on('end', function() {
                        try {
                                var parsedData = JSON.parse(data);
                                callback(parsedData.success);
                        } catch (e) {
                                callback(false);
                        }
                });
        });
}

//執行登入
exports.doLogin = function(req, res){
	verifyRecaptcha(req.body["g-recaptcha-response"], function(success) {
        if (success) {
        	mysql.query('select password,soc_code from User where email="'+req.body['user_id']+'"',function(err,rows,fields){
				if(err){
					console.log('error');
					return res.redirect('/');
				}else{
					if(rows.length==0){
						loginError = 2;
						console.log('Do not have this user');
						return res.redirect('/');
					}else if(rows[0].password!=req.body['password']){
						loginError = 1;
						console.log('Wrong Password');
						return res.redirect('/');
					}else if(rows[0].password==req.body['password']){
						console.log('logged in');
						res.cookie('userid', req.body['user_id'], { path: '/', signed: true});
						res.cookie('password', req.body['password'], { path: '/', signed: true });
						res.cookie('soc_code', rows[0].soc_code, { path: '/', signed: true });
						if(rows[0].soc_code=='admin'){
							console.log('admin');
						}else{
							console.log('normal user');
						}
						loginError = 0;
						return res.redirect('index');
					}
				}
			});
        } else {
        	loginError = 3;
			console.log('Recaptcha problem');
			return res.redirect('/');
        }
    });
	
};

exports.logout = function(req,res){
	res.clearCookie('userid', { path: '/' });
	res.clearCookie('password', { path: '/' });

	return res.redirect('/');
};


exports.notice = function(req, res){
//removed, please use controllers/notice.js 
};


exports.socinfo = function(req, res){
	checkLoginStatus(req,res);
	if(!isLogin){
		return res.redirect('login');
	}else{
		console.log('socinfo');
		res.render( 'socinfo',{
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	}
};

exports.post_notice = function(req, res){
//removed
};

exports.submit_notice = function(req,res){
//removed
};


exports.form4 = function(req, res){
	checkLoginStatus(req,res);
	if(!isLogin){
		console.log('not login');
		return res.redirect('login');
	}
	res.render('form4',{
		IsAdmin : isAdmin,
		loginStatus : isLogin
	});
};

