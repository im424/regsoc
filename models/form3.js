var mysql = require('../db');
var User = require('../models/user');
var Upload_File  = require('../models/uploadFile');
var whichForm = null;
var formNameList = [];
var whichForminFrom3 = null;
var repotType = null;
var rid = null;

exports.AdminInit = function(req,res,type_ref_no,whichForminFrom3,callback){
	switch(whichForminFrom3){
			case 'A':
				whichForm = "去屆工作報告";
				repotType = "report";
				break;
			case 'B':
				whichForm = "去屆財政報告";
				repotType = "financial_report";
				break;
			case 'C':
				whichForm = "現屆工作計劃";
				repotType = "year_plan";
				break;
			case 'D':
				whichForm = "現屆財政預算";
				repotType = "budget";
				break;
			default:
				res.write('Error - Form 3_ is missing');
				break;
		}
	mysql.query('select * from Reg_Soc_Doc where soc_code="'+req.query["soc_code"]+'" order by record_id desc limit 1',function(err,rsd,fields){
			if(err){
				console.log("1"+err);
				callback('','','');
				return;
			}else{
				var type_ref_no = repotType+"_ref_no";

				if(rsd[0] == undefined){
					console.log("no Reg_Soc_Doc record");
					callback('noRecord','','');
					return;
				}else if( rsd[0][repotType+"_ref_no"]==undefined||rsd[0][repotType+"_ref_no"]==null){ 
					console.log("4");
					callback(rsd[0],'','');
					return;
				}else{
					
				mysql.query('select * from Motion where ref_no = "'+rsd[0][type_ref_no]+'" limit 1',function(err,motion,fields){
						if(err){
							console.log("2"+err);
							callback('','','');
						}else if(motion[0]==null || motion[0]==undefined){
							console.log("\033[1;33m empty \033[0m");
							callback('','','');
							return;
						}else{
							 if(motion[0]["type"]=="meeting"){
								mysql.query('select * from AGM_Motion where motion_ref_no = "'+rsd[0][repotType+"_ref_no"]+'" limit 1',function(err,record,fields){
									if(err){
										console.log("5"+err);
										callback('','','');
										return;
									}else{
										callback(rsd[0],motion[0],record[0]);
										return;
									}
								});
							}else if(motion[0]["type"]=="vote"){
								mysql.query('select * from Vote_Motion where motion_ref_no = "'+rsd[0][repotType+"_ref_no"]+'" limit 1',function(err,record,fields){
									if(err){
										console.log("3"+err);
										callback('','','');
										return;
									}else{
										callback(rsd[0],motion[0],record[0]);
										return;
									} }); } } }); } } }); }

exports.Form3Init = function(req,res,whichForminFrom3,callback){
	switch(whichForminFrom3){
			case 'A':
				whichForm = "去屆工作報告";
				repotType = "report";
				break;
			case 'B':
				whichForm = "去屆財政報告";
				repotType = "financial_report";
				break;
			case 'C':
				whichForm = "現屆工作計劃";
				repotType = "year_plan";
				break;
			case 'D':
				whichForm = "現屆財政預算";
				repotType = "budget";
				break;
			default:
				res.write('Error - Form 3_ is missing');
				break;
		}
	mysql.query('select * from Reg_Soc_Doc where soc_code="'+req.signedCookies.soc_code+'" order by record_id desc limit 1',function(err,rsd,fields){
			if(err){
				console.log("1"+err);
				callback('','','');
				return;
			}else{
				var type_ref_no = repotType+"_ref_no";
				if(rsd[0][repotType+"_ref_no"]==undefined||rsd[0][repotType+"_ref_no"]==null){ 
					console.log("empty reportType_ref_no")
					callback(rsd[0],'','');
					return;
				}else{
					
				mysql.query('select * from Motion where ref_no = "'+rsd[0][type_ref_no]+'" limit 1',function(err,motion,fields){
						if(err){
							console.log("2"+err);
							callback('','','');
						}else if(motion[0]==null || motion[0]==undefined){
							console.log("\033[1;33m empty \033[0m");
							callback('','','');
							return;
						}else{
							 if(motion[0]["type"]=="meeting"){
								mysql.query('select * from AGM_Motion where motion_ref_no = "'+rsd[0][repotType+"_ref_no"]+'" limit 1',function(err,record,fields){
									if(err){
										console.log("2"+err);
										callback('','','');
										return;
									}else{
										callback(rsd[0],motion[0],record[0]);
										return;
									}
								});
							}else if(motion[0]["type"]=="vote"){
								mysql.query('select * from Vote_Motion where motion_ref_no = "'+rsd[0][repotType+"_ref_no"]+'" limit 1',function(err,record,fields){
									if(err){
										console.log("3"+err);
										callback('','','');
										return;
									}else{
										callback(rsd[0],motion[0],record[0]);
										return;
									} }); } } }); } } }); }


exports.submitForm3 = function(req,res,type_ref_no,whichForminFrom3,callback){
	mysql.query(' select record_id from Reg_Soc_Doc where soc_code="'+req.signedCookies.soc_code+'" order by record_id desc limit 1',function(err,rid,fields){
				total_file=2;
				formNameList[1] = 'all_report_url';
				formNameList[2] = 'vote_temp';
			if(req.body['method']!='meeting'&& req.body['method']!='vote'){
					Upload_File.uploadFileForm3(req,res,formNameList,'/soc/',1,total_file,whichForminFrom3,true,0);
					return;
				}
		mysql.query('insert into Motion(type,status,no_of_member,legal_mini_no,no_of_vote,no_of_agr,no_of_opp,no_of_abs,no_of_inv) values ("'+req.body['method']+'","'+req.body['result'][0]+'","'+req.body['totalNoOfPeople'][0]+'","'+req.body['minVote'][0]+'","'+req.body['noOfVote'][0]+'","'+req.body['agree'][0]+'","'+req.body['object'][0]+'","'+req.body['wive'][0]+'","'+req.body['void'][0]+'")',function(err,motion_record,fields){
			if(err){
				console.log(err);
			}else{
		
				
				console.log('會員大員 lower part write into DB');
				console.log('================================');
			
				if(req.body['method']=='vote'){//selected vote
					//upper part
					mysql.query('insert into Vote_Motion(motion_ref_no,poll_start_date,poll_end_date,poll_detail,poll_method,elect_commitee,scrutineer_name,scrutineer_info) values ('+motion_record.insertId+',"'+req.body['voteDateStart']+'","'+req.body['voteDateEnd']+'","'+req.body['voteInfo']+'","'+req.body['voteMethod']+'","'+req.body['commiteeOfElection']+'","'+req.body['Name'][0]+'","'+req.body['Position'][0]+'")',function(err,vote,fields){
						if(err){
							console.log(err);
						}else{
							if(rid==undefined){ 
								mysql.query('insert into Reg_Soc_Doc( soc_code, '+type_ref_no+') value("'+req.signedCookies.soc_code+'" ,'+motion_record.insertId+'); ',function(err,rows,fields){
								if(err){
									console.log(err);
								}else{
									console.log('全民投票 upper part write into DB :'+rid[0]["record_id"]);
									console.log('----------------------------------');
									//change the 'soc/' in the parameter to the folder in public/files, works even if the folder is not exist.
									Upload_File.uploadFileForm3(req,res,formNameList,'/soc/',1,total_file,whichForminFrom3,true,vote.insertId);
								}
								});

							}else{
								mysql.query('INSERT into Reg_Soc_Doc(record_id, soc_code, '+type_ref_no+') value('+rid[0]["record_id"]+', "'+req.signedCookies.soc_code+'" ,'+motion_record.insertId+') ON DUPLICATE KEY UPDATE '+type_ref_no+'='+motion_record.insertId+'; ',function(err,rows,fields){
								if(err){
									console.log(err);
								}else{
									console.log('全民投票 upper part write into DB :'+rid[0]["record_id"]);
									console.log('----------------------------------');
									//change the 'soc/' in the parameter to the folder in public/files, works even if the folder is not exist.
									Upload_File.uploadFileForm3(req,res,formNameList,'/soc/',1,total_file,whichForminFrom3,true,vote.insertId);
								}
								});
							}	
						}
					});
					//lower part
				}else if(req.body['method']=='meeting'){//selected meeting
					//upper part
					mysql.query('insert into AGM_Motion(motion_ref_no,datetime,location,chairman_name,chairman_info) values ('+motion_record.insertId+',"'+req.body['meetingTime']+'","'+req.body['meetingVen']+'","'+req.body['Name']+'","'+req.body['Position']+'")',function(err,rows,fields){
						if(err){
							console.log(err);
						}else{
							if(rid==undefined){ 
								mysql.query('insert into Reg_Soc_Doc( soc_code, '+type_ref_no+') value( "'+req.signedCookies.soc_code+'" ,'+motion_record.insertId+'); ',function(err,rows,fields){
								if(err){
									console.log(err);
								}else{
									console.log('會員大員 upper part write into DB');
									console.log('----------------------------------');
								//change the 'soc/' in the parameter to the folder in public/files, works even if the folder is not exist.
									Upload_File.uploadFileForm3(req,res,formNameList,'/soc/',1,total_file,whichForminFrom3,true,vote.insertId);
								}
								});
							}else{
								mysql.query('INSERT  into Reg_Soc_Doc(record_id, soc_code, '+type_ref_no+') value('+rid[0]["record_id"]+', "'+req.signedCookies.soc_code+'" ,'+motion_record.insertId+') ON DUPLICATE KEY UPDATE   '+type_ref_no+'='+motion_record.insertId+';',function(err,rows,fields){
								if(err){
									console.log(err);
								}else{
									console.log('會員大員 upper part write into DB');
									console.log('----------------------------------');
								//change the 'soc/' in the parameter to the folder in public/files, works even if the folder is not exist.
									Upload_File.uploadFileForm3(req,res,formNameList,'/soc/',1,total_file,whichForminFrom3,true,0);
								} }); } } }); } } }); }); }

