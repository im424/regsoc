var mysql = require('../db');

exports.Form1Init = function(req,res,callback){
	//IMPORTANT: Assuming the admin will manully add the soc_code for every new registered soc, else error will happen.
	//mysql.query('select soc_name,soc_type from Soc_info where soc_code=(select soc_code from User where email = "'+req.signedCookies.userid+'")',function(err,rows,fields,res){
	mysql.query('SELECT i.* FROM Soc_info AS i, User AS u WHERE i.soc_code = u.soc_code AND u.email ="'+req.signedCookies.userid+'"',function(err,rows,fields,res){
		if(err){
			console.log('\n \033[1;31m Can\'t Get Soc_info missing: %s \033[0m\n',req.signedCookies.userid);
			console.log(err);
			callback(' ','err');
		}else{
			console.log("\n \033[1;32m Get Soc_info: %s \033[0m\n",req.signedCookies.userid);
			if(rows.length == 0){
				console.log('\n \033[1;33m  Soc_info emtpy: %s \033[0m\n',req.signedCookies.userid);
				callback(' ',' ');
				return;
			}
			console.log(rows[0]);
			callback(rows[0]);
		}
	});
}

exports.AdminInit = function(req,res,callback){
	//IMPORTANT: Assuming the admin will manully add the soc_code for every new registered soc, else error will happen.
	//mysql.query('select soc_name,soc_type from Soc_info where soc_code=(select soc_code from User where email = "'+req.signedCookies.userid+'")',function(err,rows,fields,res){
	mysql.query('SELECT * FROM Soc_info  WHERE soc_code = "'+req.query["soc_code"]+'"',function(err,rows,fields,res){
		if(err){
			console.log('\n \033[1;31m Can\'t Get Soc_info missing(ADMIN): %s \033[0m\n',req.query["soc_code"]);
			console.log(err);
			callback(' ','err');
		}else{
			console.log("\n \033[1;32m Get Soc_info (ADMIN): %s \033[0m\n",req.query["soc_code"]);
			console.log("%j", rows);
			if(rows.length == 0){
				console.log('\n \033[1;33m  Soc_info emtpy(ADMIN): %s \033[0m\n',req.query["soc_code"]);
				callback(null);
				return;
			}
			callback(rows[0]);
		}
	});
}

exports.submitForm1 = function(req,res,callback){
	mysql.query('select soc_code from User where email="'+req.signedCookies.userid+'"',function(err,rows,fields,res){
		if(err){
			console.log(err);
		}else{
			console.log(rows[0].num_of_mem);
			//mysql.query('update Soc_Info set soc_name="'+req.body['socname']+'",soc_type="'+req.body['type']+'" where soc_code="'+rows[0].soc_code+'"',function(err,rows,fields){
			mysql.query('REPLACE INTO Soc_Info set soc_name="'+req.body['socname']+'",soc_type="'+req.body['type']+'", soc_code="'+rows[0].soc_code+'", num_of_mem='+req.body['num_of_mem'],function(err,rows,fields){
				if(err){
					console.log("\n \033[1;31m INSERT ERROR Soc_info: %s \033[0m\n",req.signedCookies.userid);
					console.log(err);
					callback(0);
				}else{
					console.log("\n \033[1;32m INSERT Soc_info: %s \033[0m\n",req.signedCookies.userid);
					callback(1);
				}
			});
		}
	});
}

