var mysql = require('../db');
var fs = require('fs');
var file_detail = {};
var totalFile = 0;
var Upload_File  = require('./uploadFile');
var CheckLogin  = require('./checkLogin');
var UFDB  = require('./uploadFileToDB');
var mkdirp = require('mkdirp');


exports.uploadFile = function (req,res,upload_field,target_des,current_file,total_file,return_to,show_page){
	if(upload_field[current_file]!=undefined){//upload_field==undefined when there is no more new field for file uploading in that form
		var url = req.files[upload_field[current_file]];
		if(url.type=='application/pdf'){//only accept pdf file?
			var tmpPath = url.path;
			console.log(tmpPath);
			var file_split = url.name.split(' ');//remove the space in the file
			var dirPath = './public/files/' + target_des;
			var targetPath = dirPath + file_split.join(''); //file location include public/files

			 //check if the directory exist, if not create one.
			 try {
			    stats = fs.lstatSync(dirPath);
			    // Is it a directory?
			    if (stats.isDirectory()) {
			    }
			}
			catch (e) {
			    //it is not a dir
			    mkdirp(dirPath, function(err) { 
			    	console.log(err);
				});
			}


		    fs.rename(tmpPath, targetPath, function(err) {
		        if(err){
		        	console.log('current_file_1: '+current_file);
		        	current_file++;
		        	Upload_File.uploadFile(req,res,upload_field,target_des,current_file,total_file,return_to,show_page);
		        }else{
			        fs.unlink(tmpPath, function() {
			           	console.log('File Uploaded to ' + targetPath + ' - ' + url.size + ' bytes');
			           	//for form 3 only--start
			     //       	if(return_to.indexOf('form3')!=-1){
			     //       		console.log('upload_field[current_file]: '+upload_field[current_file-1]+' , '+current_file);
			     //       		if(upload_field[current_file-1]=='vote_temp'){//選票樣式
				    //        		mysql.query('update Vote_Motion set ballot_temp="'+target_des + file_split.join('')+'" where vote_ref_no="'+req.body['motion'][0]+'"',function(err,rows,fields){
								// 	if(err){
								// 		console.log(err);
								// 	}else{
								// 		console.log('vote_temp upload done');
								// 	}
								// });
			     //       		}else{
				    //        	//save file location to database.
					   //         	var whichcolumn = null;
					   //         	console.log('return: '+return_to+' whichcolumn: '+whichcolumn);
					   //         	switch(return_to){//handled form 3 only
			     //       				case '/form3?form3=A':
								// 		whichcolumn = "report_url";
								// 		break;
								// 	case '/form3?form3=B':
								// 		whichcolumn = "financial_url";
								// 		break;
								// 	case '/form3?form3=C':
								// 		whichcolumn = "year_plan_url";
								// 		break;
								// 	case '/form3?form3=D':
								// 		whichcolumn = "budget_url";
								// 		break;
					   //         	}
				    //        		UFDB.uploadFileToDB(req,res,target_des + file_split.join(''),whichcolumn);
				    //        	}
			         //  	}else 
			           	if(return_to.indexOf('form1')!=-1){//for form 1 only--end

			           		if(upload_field[current_file-1]=='constitution_url'){
			           			mysql.query('select soc_code from User where email="'+req.signedCookies.userid+'"',function(err,rows,fields,res){
									if(err){
										console.log(err);
									}else{
										console.log(rows[0].num_of_mem);
										//mysql.query('update Soc_Info set soc_name="'+req.body['socname']+'",soc_type="'+req.body['type']+'" where soc_code="'+rows[0].soc_code+'"',function(err,rows,fields){
										mysql.query('update Soc_Info set constitution_ref_no="'+target_des + file_split.join('')+'" where soc_code="'+rows[0].soc_code+'"',function(err,rows,fields){
											if(err){
												console.log("\n \033[1;31m INSERT ERROR Soc_info: %s \033[0m\n",req.signedCookies.userid);
												console.log(err);
											}else{
												console.log("\n \033[1;32m INSERT Soc_info: %s \033[0m\n",req.signedCookies.userid);
											}
										});
									}
								});
			           		}else if(upload_field[current_file-1]=='member_list_url'){
			           			mysql.query('select soc_code from User where email="'+req.signedCookies.userid+'"',function(err,rows,fields,res){
									if(err){
										console.log(err);
									}else{
										console.log(rows[0].num_of_mem);
										//mysql.query('update Soc_Info set soc_name="'+req.body['socname']+'",soc_type="'+req.body['type']+'" where soc_code="'+rows[0].soc_code+'"',function(err,rows,fields){
										mysql.query('update Soc_Info set mem_list_url="'+target_des + file_split.join('')+'" where soc_code="'+rows[0].soc_code+'"',function(err,rows,fields){
											if(err){
												console.log("\n \033[1;31m INSERT ERROR Soc_info: %s \033[0m\n",req.signedCookies.userid);
												console.log(err);
											}else{
												console.log("\n \033[1;32m INSERT Soc_info: %s \033[0m\n",req.signedCookies.userid);
											}
										});
									}
								});
			           		}
			           	}
			           	
			        });
			        file_detail[upload_field[current_file]] = url.name;
			        console.log(file_detail);
			        current_file++;
				    Upload_File.uploadFile(req,res,upload_field,target_des,current_file,total_file,return_to,show_page);
			    	
		        }
		    });
		}else if(url.type!='application/pdf' && url.name){//not a pdf file and have file upload, return.
			console.log('not pdf');
			return res.redirect(return_to);
		}else{
			current_file++;
		    Upload_File.uploadFile(req,res,upload_field,target_des,current_file,total_file,return_to,show_page);
		}
	}else{
		var fileUploaded = [];
		for(var i in file_detail){
			fileUploaded.push(file_detail[i]);
		}
		fileUploaded = fileUploaded.join(' & ');
		//console.log('fileUploaded:'+fileUploaded.length);
		//reset for next time upload
		file_detail={};
		if(show_page){
	    	if(fileUploaded==''){

	    		if(return_to.indexOf('form3')!=-1){//form 3
	    			var isAdmin = CheckLogin.IsAdmin(req,res);
					var isLogin = CheckLogin.checkLoginStatus(req,res);
			    	res.render('submit_form3',{
						IsAdmin : isAdmin,
						loginStatus : isLogin,
						upload_status : '成功',
						return_path : return_to,
			    		upload_message : '資料已保存'
			    	});
	    		}else{
	    			return res.redirect(return_to);
	    		}
		    }else{
		    	var isAdmin = CheckLogin.IsAdmin(req,res);
				var isLogin = CheckLogin.checkLoginStatus(req,res);
		    	res.render('submit_form1',{
					IsAdmin : isAdmin,
					loginStatus : isLogin,
					upload_status : '上傳成功',
					return_path : return_to,
		    		upload_message : fileUploaded + ' 已經上傳成功!'
		    	});
		    	//reset json for next time upload
		    }
		}else{

		} 
	}
};

exports.uploadFileForm3 = function (req,res,upload_field,target_des,current_file,total_file,return_to,show_page,ref_no){
	if(upload_field[current_file]!=undefined){//upload_field==undefined when there is no more new field for file uploading in that form
		var url = req.files[upload_field[current_file]];
		if(url.type=='application/pdf'){//only accept pdf file?
			var tmpPath = url.path;
			console.log(tmpPath);
			var file_split = url.name.split(' ');//remove the space in the file
			var dirPath = './public/files/' + target_des;
			var targetPath = dirPath + file_split.join(''); //file location include public/files

			 //check if the directory exist, if not create one.
			 try {
			    stats = fs.lstatSync(dirPath);
			    // Is it a directory?
			    if (stats.isDirectory()) {
			    }
			}
			catch (e) {
			    //it is not a dir
			    mkdirp(dirPath, function(err) { 
			    	console.log(err);
				});
			}


		    fs.rename(tmpPath, targetPath, function(err) {
		        if(err){
		        	console.log('current_file_1: '+current_file);
		        	current_file++;
		        	Upload_File.uploadFileForm3(req,res,upload_field,target_des,current_file,total_file,return_to,show_page,ref_no);
		        }else{
			        fs.unlink(tmpPath, function() {
			           	console.log('File Uploaded to ' + targetPath + ' - ' + url.size + ' bytes');
			           	//for form 3 only--start
			      
			           	if(return_to.indexOf('form3')!=-1){
			           		console.log('upload_field[current_file]: '+upload_field[current_file-1]+' , '+current_file);
			           		if(upload_field[current_file-1]=='vote_temp'){//選票樣式
				           		mysql.query('update Vote_Motion set ballot_temp="'+target_des + file_split.join('')+'" where vote_ref_no='+ref_no,function(err,rows,fields){
									if(err){
										console.log(err);
									}else{
										console.log('vote_temp upload done %j',rows);
									}
								});
			           		}else{
				           	//save file location to database.
					           	var whichcolumn = null;
					           	console.log('return: '+return_to+' whichcolumn: '+whichcolumn);
					           	switch(return_to){//handled form 3 only
			           				case '/form3?form3=A':
										whichcolumn = "report_url";
										break;
									case '/form3?form3=B':
										whichcolumn = "financial_url";
										break;
									case '/form3?form3=C':
										whichcolumn = "year_plan_url";
										break;
									case '/form3?form3=D':
										whichcolumn = "budget_url";
										break;
					           	}
				           		UFDB.uploadFileToDB(req,res,target_des + file_split.join(''),whichcolumn);
				           	}
			           	}

			        });
			        file_detail[upload_field[current_file]] = url.name;
			        console.log(file_detail);
			        current_file++;
				    Upload_File.uploadFileForm3(req,res,upload_field,target_des,current_file,total_file,return_to,show_page,ref_no);
			    	
		        }
		    });
		}else if(url.type!='application/pdf' && url.name){//not a pdf file and have file upload, return.
			console.log('not pdf');
			return res.redirect(return_to);
		}else{
			current_file++;
		    Upload_File.uploadFileForm3(req,res,upload_field,target_des,current_file,total_file,return_to,show_page,ref_no);
		}
	}else{
		var fileUploaded = [];
		for(var i in file_detail){
			fileUploaded.push(file_detail[i]);
		}
		fileUploaded = fileUploaded.join(' & ');
		//console.log('fileUploaded:'+fileUploaded.length);
		//reset for next time upload
		file_detail={};
		if(show_page){
	    	if(fileUploaded==''){
	    		if(return_to.indexOf('form3')!=-1){//form 3
	    			var isAdmin = CheckLogin.IsAdmin(req,res);
					var isLogin = CheckLogin.checkLoginStatus(req,res);
			    	res.render('submit_form3',{
						IsAdmin : isAdmin,
						loginStatus : isLogin,
						upload_status : '成功',
						return_path : return_to,
			    		upload_message : '資料已保存'
			    	});
	    		}else{
	    			return res.redirect(return_to);
	    		}
		    }else{
		    	var isAdmin = CheckLogin.IsAdmin(req,res);
				var isLogin = CheckLogin.checkLoginStatus(req,res);
		    	res.render('submit_form1',{
					IsAdmin : isAdmin,
					loginStatus : isLogin,
					upload_status : '上傳成功',
					return_path : return_to,
		    		upload_message : fileUploaded + ' 已經上傳成功!'
		    	});
		    	//reset json for next time upload
		    }
		}else{

		} 
	}
};

