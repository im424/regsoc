var mysql = require('../db');

exports.Form4Init = function(req,res,callback){
	//IMPORTANT: Assuming the admin will manully add the soc_code for every new registered soc, else error will happen.
	//mysql.query('select soc_name,soc_type from Soc_info where soc_code=(select soc_code from User where email = "'+req.signedCookies.userid+'")',function(err,rows,fields,res){
	mysql.query('SELECT r.record_id ,r.note_by_exco FROM Reg_soc_doc AS r, User AS u WHERE r.soc_code = u.soc_code AND u.email ="'+req.signedCookies.userid+'"',function(err,rows,fields,res){
		if(err){
			console.log('\n \033[1;31m Can\'t Get Reg_soc_info missing: %s \033[0m\n',req.signedCookies.userid);
			console.log(err);
			callback(' ','err');
		}else{
			console.log("\n \033[1;32m Get Reg_soc_info %s \033[0m\n",req.signedCookies.userid);
			console.log("~!%j", rows[0]);
			if(rows.length == 0){
				console.log('\n \033[1;33m  Reg_soc_info emtpy: %s \033[0m\n',req.signedCookies.userid);
				callback(' ',' ');
				return;
			}
			callback(rows[0]);
		}
	});
}

exports.submitForm4 = function(req,res,callback){
	mysql.query('select soc_code from User where email="'+req.signedCookies.userid+'"',function(err,rows,fields,res){
		if(err){
			console.log(err);
		}else{
			console.log("\n\n\nSOC %j",rows[0].soc_code);
			//mysql.query('update Soc_Info set soc_name="'+req.body['socname']+'",soc_type="'+req.body['type']+'" where soc_code="'+rows[0].soc_code+'"',function(err,rows,fields){
			mysql.query('INSERT INTO Reg_soc_doc set note_by_exco ="' +req.body['addidtional_info']+ '", soc_code="'+  rows[0].soc_code +'"  , record_id="'+req.body['rid']+'" ON DUPLICATE KEY UPDATE  note_by_exco ="' +req.body['addidtional_info']+ '"',function(err,rows,fields){
				if(err){
					console.log("\n \033[1;31m INSERT ERROR reg_soc_info: %s \033[0m\n",req.signedCookies.userid);
					console.log(err);
					callback(0);
				}else{
					console.log("\n \033[1;32m INSERT reg_soc_info: %s \033[0m\n",req.signedCookies.userid);
					callback(1);
				}
			});
		}
	});
}

exports.AdminInit = function(req,res,callback){
	//IMPORTANT: Assuming the admin will manully add the soc_code for every new registered soc, else error will happen.
	//mysql.query('select soc_name,soc_type from Soc_info where soc_code=(select soc_code from User where email = "'+req.signedCookies.userid+'")',function(err,rows,fields,res){
	mysql.query('SELECT note_by_exco FROM Reg_soc_doc WHERE soc_code ="'+req.query["soc_code"]+'" order by record_id asc',function(err,rows,fields,res){
		if(err){
			console.log('\n \033[1;31m Can\'t Get Reg_soc_info missing: %s \033[0m\n',req.query["soc_code"]);
			console.log(err);
			callback(' ','err');
		}else{
			console.log("\n \033[1;32m Get Reg_soc_info %s \033[0m\n",req.query["soc_code"]);
			console.log("%j", rows);
			if(rows.length == 0){
				console.log('\n \033[1;33m  Reg_soc_info emtpy: %s \033[0m\n',req.query["soc_code"]);
				callback(' ',' ');
				return;
			}
			callback(rows[0]);
		}
	});
}