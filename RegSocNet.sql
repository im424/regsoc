-- phpMyAdmin SQL Dump
-- version 4.0.3
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2015 at 03:22 AM
-- Server version: 5.6.12
-- PHP Version: 5.5.14
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
--
-- Database: `regSocNet`
--
CREATE DATABASE IF NOT EXISTS `regSocNet` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `regSocNet`;
-- --------------------------------------------------------
--
-- Table structure for table `AGM_Motion`
--
CREATE TABLE IF NOT EXISTS `AGM_Motion` (
  `agm_ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `motion_ref_no` int(11) NOT NULL,
  `datetime` date DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `chairman_name` varchar(50) DEFAULT NULL,
  `chairman_info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`agm_ref_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;
--
-- Dumping data for table `AGM_Motion`
--
INSERT INTO `AGM_Motion` (`agm_ref_no`, `motion_ref_no`, `datetime`, `location`, `chairman_name`, `chairman_info`) VALUES
(1, 1, '1999-01-01', '1999/1/1', '12323', '4'),
(2, 2, '2000-12-01', '2000/2/1', 'ASD', '12'),
(3, 3, '1999-01-01', '1999/1/1', 'saf', 'sdf'),
(4, 4, '2000-12-01', '2000/2/1', 'tttt', '123o'),
(5, 5, '2000-12-01', '2000/12/1', '2000/12/1', '2000/12/1'),
(6, 6, '2000-12-01', '2000/12/1', '2000/12/1', '2000/12/1'),
(7, 7, '2000-01-01', '2000/1/1', '2000/1/1', '2000/1/1'),
(8, 8, '1999-01-01', '1999/1/1', '1999/1/1', '1999/1/1'),
(9, 9, '1999-01-01', '1999/1/1', '1999/1/1', '1999/1/1'),
(10, 10, '1999-01-01', '1999/1/1', '1999/1/1', '1999/1/1'),
(11, 11, '1999-01-01', '1999/1/1', '1999/1/1', '1999/1/1'),
(12, 12, '1999-01-01', '1999/1/1', '1999/1/1', '1999/1/1'),
(13, 13, '2000-01-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(14, 15, '2000-12-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(15, 16, '2000-11-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(16, 17, '2000-09-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(17, 18, '2000-08-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(18, 19, '2000-07-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(19, 20, '2000-06-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(20, 22, '2000-05-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(21, 23, '2000-04-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(22, 24, '2000-03-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(23, 25, '2000-02-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(24, 26, '2000-01-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(25, 27, '2032-01-01', '2000/12/1', '2000/1/1', '2000/1/1'),
(26, 28, '2013-02-02', '2000/12/1', '2000/1/1', '2000/1/1'),
(27, 29, '2013-02-02', '2000/12/1', '2000/1/1', '2000/1/1'),
(28, 33, '2013-02-02', '2000/12/1', '2000/1/1', '2000/1/1'),
(29, 34, '2013-02-02', '2000/12/1', '2000/1/1', '2000/1/1'),
(30, 35, '2013-02-02', '2000/12/1', '2000/1/1', '2000/1/1'),
(31, 37, '1983-12-03', '1983/12/3', '1983/12/3', 'v');
-- --------------------------------------------------------
--
-- Table structure for table `exco_info`
--
CREATE TABLE IF NOT EXISTS `exco_info` (
  `exco_id` int(11) NOT NULL AUTO_INCREMENT,
  `year` year(4) DEFAULT NULL,
  `exco_name` varchar(255) NOT NULL,
  `exco_start_date` varchar(255) DEFAULT NULL,
  `exco_end_date` varchar(255) DEFAULT NULL,
  `soc_code` varchar(11) NOT NULL,
  PRIMARY KEY (`exco_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;
--
-- Dumping data for table `exco_info`
--
INSERT INTO `exco_info` (`exco_id`, `year`, `exco_name`, `exco_start_date`, `exco_end_date`, `soc_code`) VALUES
(1, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(2, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(3, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(4, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(5, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(6, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(7, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(8, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(9, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(10, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(11, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(12, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(13, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(14, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(15, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(16, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(17, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(18, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(19, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(20, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1'),
(21, 2015, '幹事會內閣名稱', '2015-01-01', '2015-12-12', '1');
-- --------------------------------------------------------
--
-- Table structure for table `Exco_Member`
--
CREATE TABLE IF NOT EXISTS `Exco_Member` (
  `exco_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `exco_id` int(11) DEFAULT NULL,
  `exco_member_post` varchar(50) DEFAULT NULL,
  `exco_member_Cname` varchar(30) DEFAULT NULL,
  `exco_member_Ename` varchar(50) DEFAULT NULL,
  `exco_member_sid` varchar(10) DEFAULT NULL,
  `exco_member_college` varchar(20) DEFAULT NULL,
  `exco_member_major` varchar(50) DEFAULT NULL,
  `exco_member_year_of_study` varchar(1) DEFAULT NULL,
  `exco_member_phone` varchar(8) DEFAULT NULL,
  `exco_member_email` varchar(50) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`exco_member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `Exco_Member`
--
INSERT INTO `Exco_Member` (`exco_member_id`, `exco_id`, `exco_member_post`, `exco_member_Cname`, `exco_member_Ename`, `exco_member_sid`, `exco_member_college`, `exco_member_major`, `exco_member_year_of_study`, `exco_member_phone`, `exco_member_email`, `submit_timestamp`) VALUES
(1, 1, 'asd', '︱翁︱剃', 'asd', '13123', 'CC', 'sdf', '3', '123', NULL, '2015-04-06 10:02:39'),
(2, 1, 'asd', '︱翁︱剃', 'asd', '13123', 'CC', 'sdf', '3', '123', NULL, '2015-04-06 10:02:39'),
(3, 2, 'chairman', '少鏽考', 'GG', '13453423', 'CC', 'ENGG', '2', '23452354', NULL, '2015-04-06 11:02:39');
-- --------------------------------------------------------
--
-- Table structure for table `IM`
--
CREATE TABLE IF NOT EXISTS `IM` (
  `chatwith` varchar(10) NOT NULL,
  `person` varchar(10) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Dumping data for table `IM`
--
INSERT INTO `IM` (`chatwith`, `person`, `message`, `time`) VALUES
('1', '1', 'hi', '2015-03-30 06:19:29'),
('1', 'admin', 'asd', '2015-03-30 06:19:36'),
('1', 'admin', 'asdsd', '2015-03-30 06:20:10'),
('2', '2', 'asdkjasd', '2015-03-30 06:22:24'),
('2', 'admin', 'hi', '2015-03-30 06:22:27'),
('1', '1', 'hi admin', '2015-04-01 08:43:07'),
('1', '1', 'hi', '2015-04-01 08:43:10'),
('1', '1', 'hi', '2015-04-01 08:43:11');
-- --------------------------------------------------------
--
-- Table structure for table `Motion`
--
CREATE TABLE IF NOT EXISTS `Motion` (
  `ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(25) NOT NULL,
  `status` varchar(30) DEFAULT NULL,
  `no_of_member` int(11) DEFAULT NULL,
  `legal_mini_no` int(11) DEFAULT NULL,
  `no_of_vote` int(11) DEFAULT NULL,
  `no_of_agr` int(11) DEFAULT NULL,
  `no_of_opp` int(11) DEFAULT NULL,
  `no_of_abs` int(11) DEFAULT NULL,
  `no_of_inv` int(11) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ref_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;
--
-- Dumping data for table `Motion`
--
INSERT INTO `Motion` (`ref_no`, `type`, `status`, `no_of_member`, `legal_mini_no`, `no_of_vote`, `no_of_agr`, `no_of_opp`, `no_of_abs`, `no_of_inv`, `submit_timestamp`) VALUES
(1, 'meeting', 'pass', 3, 2, 3, 3, 2, 0, 0, '2015-04-08 20:16:43'),
(2, 'meeting', 'pass', 3, 2, 2, 3, 2, 1, 5, '2015-04-08 20:22:27'),
(3, 'meeting', 'pass', 2, 2, 2, 2, 0, 0, 0, '2015-04-08 20:27:52'),
(4, 'meeting', 'pass', 1, 9, 9, 1, 1, 3, 2, '2015-04-08 20:30:24'),
(5, 'meeting', 'pass', 1, 3, 2, 1, 1, 1, 1, '2015-04-08 20:31:32'),
(6, 'meeting', 'pass', 1, 1, 1, 1, 2, 3, 2, '2015-04-08 20:32:43'),
(7, 'meeting', 'pass', 1, 1, 2, 2, 2, 2, 2, '2015-04-08 20:33:45'),
(8, 'meeting', 'pass', 1, 1, 1, 1, 2, 1, 3, '2015-04-08 20:51:53'),
(9, 'meeting', 'pass', 1, 1, 1, 1, 2, 1, 3, '2015-04-08 20:51:53'),
(10, 'meeting', 'pass', 1, 1, 1, 1, 2, 1, 3, '2015-04-08 20:52:48'),
(11, 'meeting', 'pass', 1, 1, 1, 1, 2, 1, 3, '2015-04-08 20:53:44'),
(12, 'meeting', 'pass', 1, 1, 3, 1, 2, 3, 4, '2015-04-08 20:54:49'),
(13, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 21:04:37'),
(14, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:00:47'),
(15, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:08:17'),
(16, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:08:59'),
(17, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:09:21'),
(18, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:09:40'),
(19, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:12:51'),
(20, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:19'),
(21, 'other', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:23'),
(22, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:29'),
(23, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:34'),
(24, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:38'),
(25, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:43'),
(26, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:13:47'),
(27, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:16:20'),
(28, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:16:29'),
(29, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:16:42'),
(30, 'other', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:27:02'),
(31, 'other', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:31:00'),
(32, 'other', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:31:20'),
(33, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:32:29'),
(34, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:33:19'),
(35, 'meeting', 'pass', 2, 2, 3, 3, 2, 2, 2, '2015-04-08 22:35:00'),
(36, 'vote', 'fail', 123, 73, 119, 43, 2, 21, 24, '2015-04-08 22:39:09'),
(37, 'meeting', 'pass', 1, 1, 1, 1, 1, 1, 1, '2015-04-08 23:55:23'),
(38, 'vote', 'pass', 12, 32, 12, 31, 123, 112, 23, '2015-04-09 00:41:44');
-- --------------------------------------------------------
--
-- Table structure for table `Notice`
--
CREATE TABLE IF NOT EXISTS `Notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notice_content` text,
  `post_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;
--
-- Dumping data for table `Notice`
--
INSERT INTO `Notice` (`notice_id`, `title`, `notice_content`, `post_timestamp`, `edit_timestamp`) VALUES
(1, 'Title 1', '1 notice_content', '2015-03-13 02:20:15', '2015-03-15 16:03:15'),
(2, 'title 2', '2', '2015-03-13 02:20:19', '0000-00-00 00:00:00'),
(3, 'title 3', '3', '2015-03-13 02:20:23', '0000-00-00 00:00:00'),
(4, 'Title 4', '4notice_content', '2015-03-13 02:20:26', '2015-03-15 16:03:25'),
(5, 'kjdsfhaskdjf', 'sdfiosdhkjvsnvsdb', '2015-03-16 04:25:18', '0000-00-00 00:00:00'),
(6, 'jdkfksdjfsds', 'sdfsjdhfksdj', '2015-03-16 04:26:13', '0000-00-00 00:00:00');
-- --------------------------------------------------------
--
-- Table structure for table `Reg_Soc_Doc`
--
CREATE TABLE IF NOT EXISTS `Reg_Soc_Doc` (
  `record_id` int(11) NOT NULL,
  `soc_code` varchar(10) NOT NULL,
  `exco_elect_ref_no` int(11) DEFAULT NULL,
  `year_plan_ref_no` int(11) DEFAULT NULL,
  `budget_ref_no` int(11) DEFAULT NULL,
  `report_ref_no` int(11) DEFAULT NULL,
  `financial_report_ref_no` int(11) DEFAULT NULL,
  `year_plan_url` varchar(255) DEFAULT NULL,
  `budget_url` varchar(255) DEFAULT NULL,
  `report_url` varchar(255) DEFAULT NULL,
  `financial_url` varchar(255) DEFAULT NULL,
  `note_by_exco` text,
  `note_by_committee` text,
  `reg_soc_status` varchar(30) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `1st_process_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `2nd_process_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Dumping data for table `Reg_Soc_Doc`
--
INSERT INTO `Reg_Soc_Doc` (`record_id`, `soc_code`, `exco_elect_ref_no`, `year_plan_ref_no`, `budget_ref_no`, `report_ref_no`, `financial_report_ref_no`, `year_plan_url`, `budget_url`, `report_url`, `financial_url`, `note_by_exco`, `note_by_committee`, `reg_soc_status`, `submit_timestamp`, `1st_process_timestamp`, `2nd_process_timestamp`) VALUES
(1, '1', NULL, 38, NULL, NULL, NULL, '/soc/學生團體財務須知.pdf', NULL, '/soc/ACS_Activity_Fund.pdf', NULL, NULL, NULL, NULL, '2015-04-09 00:54:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
-- --------------------------------------------------------
--
-- Table structure for table `Reg_Soc_Form`
--
CREATE TABLE IF NOT EXISTS `Reg_Soc_Form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form1` varchar(255) COLLATE utf8_bin NOT NULL,
  `form2` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3a` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3b` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3c` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3d` varchar(255) COLLATE utf8_bin NOT NULL,
  `form4` varchar(255) COLLATE utf8_bin NOT NULL,
  `note` text COLLATE utf8_bin NOT NULL,
  `admin_user_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `regsoc_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `overall` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;
--
-- Dumping data for table `Reg_Soc_Form`
--
INSERT INTO `Reg_Soc_Form` (`id`, `form1`, `form2`, `form3a`, `form3b`, `form3c`, `form3d`, `form4`, `note`, `admin_user_id`, `regsoc_id`, `timestamp`, `overall`) VALUES
(1, 'undefined', 'on', 'on', 'undefined', 'on', 'on', 'on', 'asdf', 'admin@gmail.com', '1', '2015-04-09 02:57:42', 'on');
-- --------------------------------------------------------
--
-- Table structure for table `soc_info`
--
CREATE TABLE IF NOT EXISTS `soc_info` (
  `soc_code` varchar(10) NOT NULL,
  `soc_name` varchar(255) DEFAULT NULL,
  `constitution_ref_no` varchar(255) DEFAULT NULL,
  `soc_type` varchar(10) DEFAULT NULL,
  `note_by_committee` varchar(255) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `num_of_mem` int(11) DEFAULT NULL,
  `mem_list_url` varchar(255) DEFAULT NULL,
  `exco_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`soc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Dumping data for table `soc_info`
--
INSERT INTO `soc_info` (`soc_code`, `soc_name`, `constitution_ref_no`, `soc_type`, `note_by_committee`, `submit_timestamp`, `num_of_mem`, `mem_list_url`, `exco_id`) VALUES
('1', '計算機科學系會', NULL, 'dept', NULL, '2015-04-08 15:51:39', 29, NULL, NULL),
('2', '工程學院院會', NULL, 'facu', NULL, '2015-04-06 10:17:16', 40, NULL, 2);
-- --------------------------------------------------------
--
-- Table structure for table `User`
--
CREATE TABLE IF NOT EXISTS `User` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `soc_code` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Dumping data for table `User`
--
INSERT INTO `User` (`user_id`, `email`, `password`, `soc_code`) VALUES
(1, 'admin@gmail.com', 'abcd1234', 'admin'),
(2, 'user@gmail.com', 'abcd1234', '1'),
(3, 'user1@gmail.com', 'abcd1234', '2'),
(4, 'test', 'test', '3');
-- --------------------------------------------------------
--
-- Table structure for table `Vote_Motion`
--
CREATE TABLE IF NOT EXISTS `Vote_Motion` (
  `vote_ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `motion_ref_no` int(11) NOT NULL,
  `poll_start_date` date DEFAULT NULL,
  `poll_end_date` date DEFAULT NULL,
  `poll_detail` varchar(255) DEFAULT NULL,
  `poll_method` varchar(50) DEFAULT NULL,
  `ballot_temp` varchar(255) DEFAULT NULL,
  `elect_commitee` varchar(255) DEFAULT NULL,
  `scrutineer_name` varchar(50) DEFAULT NULL,
  `scrutineer_info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vote_ref_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;
--
-- Dumping data for table `Vote_Motion`
--
INSERT INTO `Vote_Motion` (`vote_ref_no`, `motion_ref_no`, `poll_start_date`, `poll_end_date`, `poll_detail`, `poll_method`, `ballot_temp`, `elect_commitee`, `scrutineer_name`, `scrutineer_info`) VALUES
(1, 36, '2013-02-02', '2014-05-01', '需列明日期、時間、地點', '投票方式', '/soc/新亞學生會會章(20150227).pdf', 'asdq212', '123', '123'),
(2, 38, '2014-01-01', '2015-01-01', 'asdkjfogsoeritu', 'sdifuoi', '/soc/tuto06.pdf', ' qwe13 osii3sdf ', '123123', '25345');
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;