//module dependencies
var express = require('express'); //從local取得express
var routes = require('./routes'); //等同於"./routes/index.js"，指定路徑返回內容，相當於MVC中的Controller
var http = require('http');
var path = require('path');
var app = express();

var connection = require('./db');

//預設port號 3000，所以執行的URL為 http://localhost:3000
app.set('port', process.env.PORT || 3000);


app.set('views', path.join(__dirname, 'views'));//設計頁面模板位置，在views子目錄下
app.set('view engine', 'ejs');//表明要使用的模板引擎(樣板引擎，Template Engine)是ejs
// app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.bodyParser());//解析client端請求，通常是透過POST發送的內容
app.use(express.cookieParser('123456789'));//記得設定key來傳遞資訊
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// app.use(multer({ dest: './public/files/',
//  rename: function (fieldname, filename) {
//     return filename+Date.now();
//   },
// onFileUploadStart: function (file,req,res) {
//   console.log(file.originalname + ' is starting ...')
// },
// onFileUploadComplete: function (file,req,res) {
//   console.log(file.fieldname + ' uploaded to  ' + file.path)
//   done=true;
// }
// }));



// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.post('/', routes.doLogin);

app.get('/index', routes.index);
app.post('/index', routes.doLogin);

app.get('/login', routes.login);
app.post('/login', routes.doLogin);

app.get('/logout', routes.logout);

//app.get('/notice', routes.notice);

var RegSoc = require('./controllers/adminRegsoc');
// app.get('/regsoc', routes.regsoc);
// app.get('/regsoc1',routes.regsoc1);
app.get('/regsoc', RegSoc.RegSocIndex);
app.get('/regsoc1', RegSoc.regsoc);
app.get('/regsocform', RegSoc.RegSocForm);
app.post('/submit_regsocform', RegSoc.submit_RegSocForm);

var SocInfo = require('./controllers/socinfo');
app.get('/socinfo',SocInfo.EnterSocInfo);

var form1 = require('./controllers/form1');
var form2 = require('./controllers/form2');
var form3 = require('./controllers/form3');
var form4 = require('./controllers/form4');

app.get('/form1',form1.Enterform1);
app.get('/form2',form2.Enterform2);
app.get('/form3',form3.Enterform3);
app.get('/form4',form4.Enterform4);

//forms upload
app.post('/submit_form1',form1.submit_form1);
app.post('/submit_form2',form2.submit_form2);
app.post('/submit_form3',form3.submit_form3);
app.post('/submit_form4',form4.submit_form4);

//admin
app.get('/admin_regsoc_form1', form1.adminform1);
app.get('/admin_regsoc_form2', form2.adminform2);
app.get('/admin_regsoc_form3', form3.adminform3);
app.get('/admin_regsoc_form4', form4.adminform4);

var notice = require('./controllers/notice');
app.get('/notice', notice.getPost);
app.get('/post_notice',notice.postNotice);
app.post('/submit_notice',notice.submitNotice);

//download
app.get('/dl', function(req, res){
	//localhost:3000/dl?d=
	var file_location = '/public/files'+req.query.d;
	var file = __dirname + file_location;
	res.download(file); // Set disposition and send it.
});

//email
var Email = require('./controllers/email');
app.get('/email',Email.Enteremail);
app.get('/send',Email.Sendemail);
app.get('/fsend',Email.SendFemail);

//Gudeline
var GuideLine = require('./controllers/guideline');
app.get('/guideline',GuideLine.EnterGuideLine);

//contact_us
var Contact = require('./controllers/contact_us');
app.get('/contact_us',Contact.EnterContact);




var io = require('socket.io').listen(http.createServer(app).listen(app.get('port'), function( req, res ){ 
	console.log('Express server listening on port ' + app.get('port'));
}));

var users = {};
var online = {};

connection.query('select soc_code from User',function(err,rows,fields){
	if(err){
		console.log(err);
	}else{
		for(var i=0;i<rows.length;i++){
			users[rows[i].soc_code] = null;
		}
	}
});

io.sockets.on('connection', function(socket){

	socket.on('new user', function(data){
		socket.nickname = data;
		console.log('data: '+data);
		online[socket.nickname] = socket;
		users[socket.nickname] = socket;
		updateNicknames();

		if(socket.nickname != 'admin')
			getOldMsg(socket.nickname);
		else
			getOldMsg();

	});
	
	function updateNicknames(){
		io.sockets.emit('usernames', Object.keys(online));
	}

	socket.on('send message', function(data, callback){
		var msg = data.trim();
		if(socket.nickname!='admin'){//分online 同唔係online
			if(msg!=''){
				if(UserExist('admin',Object.keys(online))){//online
					users['admin'].emit('new message', {msg: msg, nick: socket.nickname});
				}else{
					console.log('admin is not online');
				}
				users[socket.nickname].emit('new message', {msg: msg, nick: socket.nickname});
				writeMsgToDB(socket.nickname,socket.nickname,msg);
			}
		}else{
			var ind = msg.indexOf(' ');
			if(ind !== -1){
				var name = msg.substring(0, ind);
				var msg = msg.substring(ind + 1);
				if(UserExist(name,Object.keys(users))){
					if(UserExist(name,Object.keys(online))){//online
						users[name].emit('new message', {msg: msg, nick: socket.nickname});
						console.log('message sent is: ' + msg);
					}else{//not online
						console.log('user not online');
					}
					users['admin'].emit('new message', {msg: msg, nick: socket.nickname,sayTo: name});
					writeMsgToDB(name,'admin',msg);
				}else{
					callback('Error! do not have this user!');
				}
			} else{
				callback('Error!  Please indicate the person you want to chat with.');
			}
		}

	});

	function getOldMsg(name){
		if(name){
			connection.query('select person,message from IM where chatwith="'+name+'"',function(err,rows,fields){
				if(err){
					console.log(err);
				}else{
					for(var i=0; i < rows.length; i++){
						console.log(rows[i].message);
					}
					socket.emit('load old msgs', rows);
				}
			});
		}else{//admin
			connection.query('select person,message,chatwith from IM',function(err,rows,fields){
				if(err){
					console.log(err);
				}else{
					for(var i=0; i < rows.length; i++){
						console.log(rows[i].message);
					}
					socket.emit('load old msgs', rows);
				}
			});
		}
			
	}
	
	function writeMsgToDB(chatwith,person,message){
		connection.query('insert into IM(chatwith,person,message) values("'+chatwith+'","'+person+'","'+message+'")',function(err,rows,fields){
			if(err){
				console.log(err);
			}
		});
	}

	function UserExist(name,list){
		for(var i=0;i<list.length;i++){
			console.log(list[i]);
			if(list[i]==name)
				return true;
		}
		return false;
	}

	socket.on('disconnect', function(data){
		if(!socket.nickname) return;
		users[socket.nickname] = null;
		delete online[socket.nickname];
		updateNicknames();
	});
});



